plugins {
    kotlin("jvm")
}

repositories {
    mavenLocal()
    mavenCentral()
    maven("https://gitlab.com/api/v4/groups/9631292/-/packages/maven")
    maven("https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    compileOnly(libs.kotlinReflect)
    compileOnly(libs.kotlinxCoroutines)

    compileOnly(libs.configurateCore)

    implementation(libs.libBulletJme)


    testImplementation(platform("org.junit:junit-bom:5.9.0"))
}
