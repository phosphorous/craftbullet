package com.gitlab.aecsocket.craftbullet.core

import com.simsilica.mathd.Vec3d

class BoundingBoxDp(
    val center: Vec3d,
    val halfExtent: Vec3d
) {
    fun min() = center - halfExtent

    fun max() = center + halfExtent
}
