package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bounding.BoundingBox
import com.jme3.bullet.collision.PhysicsCollisionObject
import com.jme3.bullet.collision.PhysicsRayTestResult
import com.jme3.bullet.collision.shapes.BoxCollisionShape
import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.collision.shapes.CompoundCollisionShape
import com.jme3.bullet.collision.shapes.infos.ChildCollisionShape
import com.jme3.bullet.objects.PhysicsBody
import com.jme3.bullet.objects.PhysicsGhostObject
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.bullet.objects.infos.RigidBodyMotionState
import com.jme3.math.*
import com.simsilica.mathd.Matrix3d
import com.simsilica.mathd.Quatd
import com.simsilica.mathd.Vec3d
import kotlin.math.*

const val STATIC_MASS: Float = PhysicsBody.massForStatic

// math

const val PI2 = PI * 2

const val FPI = PI.toFloat()
const val FPI2 = PI.toFloat() * 2

fun radians(degrees: Float): Float = degrees * (FPI / 180f)
fun radians(degrees: Double): Double = degrees * (PI / 180.0)

fun degrees(radians: Float): Float = radians * (180f / FPI)
fun degrees(radians: Double): Double = radians * (180.0 / PI)

operator fun Vector3f.plus(v: Vector3f)  = Vector3f(x + v.x, y + v.y, z + v.z)
operator fun Vector3f.plus(s: Float)     = Vector3f(x + s,   y + s,   z + s)
operator fun Vector3f.minus(v: Vector3f) = Vector3f(x - v.x, y - v.y, z - v.z)
operator fun Vector3f.minus(s: Float)    = Vector3f(x - s,   y - s,   z - s)
operator fun Vector3f.times(v: Vector3f) = Vector3f(x * v.x, y * v.y, z * v.z)
operator fun Vector3f.times(s: Float)    = Vector3f(x * s,   y * s,   z * s)
operator fun Vector3f.div(v: Vector3f)   = Vector3f(x / v.x, y  /v.y, z / v.z)
operator fun Vector3f.div(s: Float)      = Vector3f(x / s,   y / s,   z / s)
operator fun Vector3f.unaryMinus()       = Vector3f(-x, -y, -z)
operator fun Vector3f.component1()       = x
operator fun Vector3f.component2()       = y
operator fun Vector3f.component3()       = z
fun Vector3f.dp() = Vec3d(this)

operator fun Vec3d.plus(v: Vec3d)   = Vec3d(x + v.x, y + v.y, z + v.z)
operator fun Vec3d.plus(s: Double)  = Vec3d(x + s, y + s, z + s)
operator fun Vec3d.minus(v: Vec3d)  = Vec3d(x - v.x, y - v.y, z - v.z)
operator fun Vec3d.minus(s: Double) = Vec3d(x - s, y - s, z - s)
operator fun Vec3d.times(v: Vec3d)  = Vec3d(x * v.x, y * v.y, z * v.z)
operator fun Vec3d.times(s: Double) = Vec3d(x * s, y * s, z * s)
operator fun Vec3d.div(v: Vec3d)    = Vec3d(x / v.x, y / v.y, z / v.z)
operator fun Vec3d.div(s: Double)   = Vec3d(x / s, y / s, z / s)
operator fun Vec3d.unaryMinus()     = Vec3d(-x, -y, -z)
operator fun Vec3d.component1()     = x
operator fun Vec3d.component2()     = y
operator fun Vec3d.component3()     = z
fun Vec3d.sp(): Vector3f = toVector3f()

fun Quaternion.dp() = Quatd(this)
fun Quatd.sp(): Quaternion = toQuaternion()

fun Transform.dp() = TransformDp(translation.dp(), rotation.dp())
fun TransformDp.sp() = Transform(translation.sp(), rotation.sp())

fun Transform.transform(vec: Vector3f) = transformVector(vec, Vector3f())

fun Transform.transformInverse(vec: Vector3f) = transformInverseVector(vec, Vector3f())

fun TransformDp.transform(vec: Vec3d) = transformVector(vec, Vec3d())

fun TransformDp.transformInverse(vec: Vec3d) = transformInverseVector(vec, Vec3d())

// note: this is all in !!! ZYX !!! order, because this is what Minecraft uses

private fun clamp(value: Float, min: Float, max: Float) = min(max, max(min, value))

private fun clamp(value: Double, min: Double, max: Double) = min(max, max(min, value))

private const val ONE_EPSILON = 0.999999

fun Vector3f.quaternion(): Quaternion {
    fun trig(ang: Float) = sin(ang / 2) to cos(ang / 2)

    val (s1, c1) = trig(x)
    val (s2, c2) = trig(y)
    val (s3, c3) = trig(z)

    return Quaternion(
        s1*c2*c3 - c1*s2*s3,
        c1*s2*c3 + s1*c2*s3,
        c1*c2*s3 - s1*s2*c3,
        c1*c2*c3 + s1*s2*s3,
    )
}

fun Vec3d.quaternion(): Quatd {
    fun trig(ang: Double) = sin(ang / 2) to cos(ang / 2)

    val (s1, c1) = trig(x)
    val (s2, c2) = trig(y)
    val (s3, c3) = trig(z)

    return Quatd(
        s1*c2*c3 - c1*s2*s3,
        c1*s2*c3 + s1*c2*s3,
        c1*c2*s3 - s1*s2*c3,
        c1*c2*c3 + s1*s2*s3,
    )
}

fun CMatrix3f.bullet() = Matrix3f(
    m00, m01, m02,
    m10, m11, m12,
    m20, m21, m22
)

fun CMatrix3d.bullet() = Matrix3d(
    m00, m01, m02,
    m10, m11, m12,
    m20, m21, m22
)

fun CMatrix3f.euler(): Vector3f {
    val y = asin(clamp(m20, -1f, 1f))
    return if (abs(m20) < ONE_EPSILON) Vector3f(
        atan2(-m21, m22),
        y,
        atan2(-m10, m00)
    ) else Vector3f(
        atan2(m12, m11),
        y,
        0f
    )
}

fun CMatrix3d.euler(): Vec3d {
    val y = asin(clamp(m20, -1.0, 1.0))
    return if (abs(m20) < ONE_EPSILON) Vec3d(
        atan2(-m21, m22),
        y,
        atan2(-m10, m00)
    ) else Vec3d(
        atan2(m12, m11),
        y,
        0.0
    )
}

fun Quaternion.lengthSq() = (x * x) + (y * y) + (z * z) + (w * w);

fun Quaternion.matrix(): CMatrix3f {
    val d = lengthSq()
    val s = 2 / d

    val xs = x * s
    val ys = y * s
    val zs = z * s
    val xx = x * xs
    val xy = x * ys
    val xz = x * zs
    val xw = w * xs
    val yy = y * ys
    val yz = y * zs
    val yw = w * ys
    val zz = z * zs
    val zw = w * zs

    // using s=2/norm (instead of 1/norm) saves 9 multiplications by 2 here
    val m00 = 1 - (yy + zz)
    val m01 = (xy - zw)
    val m02 = (xz + yw)
    val m10 = (xy + zw)
    val m11 = 1 - (xx + zz)
    val m12 = (yz - xw)
    val m20 = (xz - yw)
    val m21 = (yz + xw)
    val m22 = 1 - (xx + yy)

    return CMatrix3f(
        m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22
    )
}

fun Quatd.matrix(): CMatrix3d {
    val d = lengthSq()
    val s = 2 / d

    val xs = x * s
    val ys = y * s
    val zs = z * s
    val xx = x * xs
    val xy = x * ys
    val xz = x * zs
    val xw = w * xs
    val yy = y * ys
    val yz = y * zs
    val yw = w * ys
    val zz = z * zs
    val zw = w * zs

    // using s=2/norm (instead of 1/norm) saves 9 multiplications by 2 here
    val m00 = 1 - (yy + zz)
    val m01 = (xy - zw)
    val m02 = (xz + yw)
    val m10 = (xy + zw)
    val m11 = 1 - (xx + zz)
    val m12 = (yz - xw)
    val m20 = (xz - yw)
    val m21 = (yz + xw)
    val m22 = 1 - (xx + yy)

    return CMatrix3d(
        m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22
    )
}

fun Quaternion.euler() = matrix().euler()

fun Quatd.euler() = matrix().euler()


val BoundingBox.center: Vector3f
    get() = getCenter(Vector3f())

val BoundingBox.halfExtents: Vector3f
    get() = getExtent(Vector3f())

fun BoundingBox.min(): Vector3f = getMin(Vector3f())

fun BoundingBox.max(): Vector3f = getMax(Vector3f())

fun BoundingBox.inflated(x: Float, y: Float, z: Float) =
    BoundingBox(center, xExtent + x, yExtent + y, zExtent + z)

fun BoundingBox.inflated(size: Float) = inflated(size, size, size)


fun BoundingBoxDp.inflated(size: Vec3d) =
    BoundingBoxDp(center, halfExtent + size)

fun BoundingBoxDp.inflated(size: Double) = inflated(Vec3d(size, size, size))

// accessors

val BoxCollisionShape.halfExtents: Vec3d
    get() = getHalfExtents(Vector3f()).dp()

val ChildCollisionShape.transform: TransformDp
    get() = copyTransform(Transform()).dp()

val PhysicsRayTestResult.hitNormal: Vec3d
    get() = getHitNormalLocal(Vector3f()).dp()

var PhysicsRigidBody.physPosition: Vec3d
    get() = getPhysicsLocationDp(Vec3d())
    set(value) { setPhysicsLocationDp(value) }
var PhysicsRigidBody.physRotation: Quatd
    get() = getPhysicsRotationDp(Quatd())
    set(value) { setPhysicsRotationDp(value) }
var PhysicsRigidBody.transform: TransformDp
    get() = getTransformDp(TransformDp())
    set(value) {
        physPosition = value.translation
        physRotation = value.rotation
    }
var PhysicsRigidBody.linearVelocity: Vec3d
    get() = getLinearVelocityDp(Vec3d())
    set(value) { setLinearVelocityDp(value) }
var PhysicsRigidBody.angularVelocity: Vec3d
    get() = getAngularVelocityDp(Vec3d())
    set(value) { setAngularVelocityDp(value) }
var PhysicsRigidBody.angularFactor: Vec3d
    get() = getAngularFactor(Vector3f()).dp()
    set(value) { setAngularFactor(value.sp()) }

var PhysicsGhostObject.physPosition: Vec3d
    get() = getPhysicsLocationDp(Vec3d())
    set(value) { setPhysicsLocationDp(value) }
var PhysicsGhostObject.physRotation: Quatd
    get() = getPhysicsRotationDp(Quatd())
    set(value) { setPhysicsRotationDp(value) }
var PhysicsGhostObject.transform: TransformDp
    get() = getTransformDp(TransformDp())
    set(value) {
        physPosition = value.translation
        physRotation = value.rotation
    }

val RigidBodyMotionState.position: Vec3d
    get() = getLocationDp(Vec3d())
val RigidBodyMotionState.rotation: Quatd
    get() = Quaternion().also { getOrientation(it) }.dp()
val RigidBodyMotionState.transform: TransformDp
    get() = TransformDp(position, rotation)

var PhysicsCollisionObject.physPosition: Vec3d
    get() = getPhysicsLocationDp(Vec3d())
    set(value) {
        if (this is PhysicsRigidBody) physPosition = value
        if (this is PhysicsGhostObject) physPosition = value
    }
var PhysicsCollisionObject.physRotation: Quatd
    get() = getPhysicsRotationDp(Quatd())
    set(value) {
        if (this is PhysicsRigidBody) physRotation = value
        if (this is PhysicsGhostObject) physRotation = value
    }
var PhysicsCollisionObject.transform: TransformDp
    get() = getTransformDp(TransformDp())
    set(value) {
        physPosition = value.translation
        physRotation = value.rotation
    }

fun PhysicsCollisionObject.boundingBox(): BoundingBoxDp {
    val box = collisionShape.boundingBox(Vector3f.ZERO, getPhysicsRotationMatrix(Matrix3f()), BoundingBox())
    return BoundingBoxDp(box.center.dp() + physPosition, box.halfExtents.dp())
}


fun CompoundCollisionShape.addShape(shape: CollisionShape, transform: Transform): Int {
    return if (shape is CompoundCollisionShape) {
        val children = shape.listChildren()
        children.forEach { child ->
            addChildShape(child.shape, child.transform.sp().combineWithParent(transform))
        }
        children.size
    } else {
        addChildShape(shape, transform)
        1
    }
}
