package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bullet.collision.ManifoldPoints
import com.jme3.bullet.collision.PersistentManifolds
import com.jme3.bullet.collision.PhysicsCollisionObject
import com.jme3.math.Vector3f
import com.simsilica.mathd.Vec3d

class ContactManifold(val id: Long) {
    val bodyAId get() = PersistentManifolds.getBodyAId(id)
    val bodyBId get() = PersistentManifolds.getBodyBId(id)

    val bodyA get() = PhysicsCollisionObject.findInstance(bodyAId)
    val bodyB get() = PhysicsCollisionObject.findInstance(bodyBId)

    val numPoints get() = PersistentManifolds.countPoints(id)

    fun point(index: Int) = ContactManifoldPoint(PersistentManifolds.getPointId(id, index))
}

@JvmInline
value class ContactManifoldPoint(val id: Long) {
    val lifetime get() = ManifoldPoints.getLifeTime(id)

    val partIdA get() = ManifoldPoints.getPartId0(id)
    val partIdB get() = ManifoldPoints.getPartId1(id)

    val indexIdA get() = ManifoldPoints.getIndex0(id)
    val indexIdB get() = ManifoldPoints.getIndex1(id)

    var flags: Int
        get() = ManifoldPoints.getFlags(id)
        set(value) { ManifoldPoints.setFlags(id, value) }

    var impulse: Float
        get() = ManifoldPoints.getAppliedImpulse(id)
        set(value) { ManifoldPoints.setAppliedImpulse(id, value) }

    var positionLocalA: Vector3f
        get() = Vector3f().also { ManifoldPoints.getLocalPointA(id, it) }
        set(value) { ManifoldPoints.setLocalPointA(id, value) }
    var positionLocalB: Vector3f
        get() = Vector3f().also { ManifoldPoints.getLocalPointB(id, it) }
        set(value) { ManifoldPoints.setLocalPointB(id, value) }

    // val because we have no DP set methods
    val positionWorldA: Vec3d
        get() = Vec3d().also { ManifoldPoints.getPositionWorldOnADp(id, it) }
    val positionWorldB: Vec3d
        get() = Vec3d().also { ManifoldPoints.getPositionWorldOnBDp(id, it) }

    var normalWorld: Vector3f
        get() = Vector3f().also { ManifoldPoints.getNormalWorldOnB(id, it) }
        set(value) { ManifoldPoints.setNormalWorldOnB(id, value) }

    var lateralFrictionDirA: Vector3f
        get() = Vector3f().also { ManifoldPoints.getLateralFrictionDir1(id, it) }
        set(value) { ManifoldPoints.setLateralFrictionDir1(id, value) }
    var lateralFrictionDirB: Vector3f
        get() = Vector3f().also { ManifoldPoints.getLateralFrictionDir2(id, it) }
        set(value) { ManifoldPoints.setLateralFrictionDir2(id, value) }

    var impulseLateralA: Float
        get() = ManifoldPoints.getAppliedImpulseLateral1(id)
        set(value) { ManifoldPoints.setAppliedImpulseLateral1(id, value) }
    var impulseLateralB: Float
        get() = ManifoldPoints.getAppliedImpulseLateral2(id)
        set(value) { ManifoldPoints.setAppliedImpulseLateral2(id, value) }

    var combinedFriction: Float
        get() = ManifoldPoints.getCombinedFriction(id)
        set(value) { ManifoldPoints.setCombinedFriction(id, value) }
    var combinedRollingFriction: Float
        get() = ManifoldPoints.getCombinedRollingFriction(id)
        set(value) { ManifoldPoints.setCombinedRollingFriction(id, value) }
    var combinedSpinningFriction: Float
        get() = ManifoldPoints.getCombinedSpinningFriction(id)
        set(value) { ManifoldPoints.setCombinedSpinningFriction(id, value) }
    var combinedRestitution: Float
        get() = ManifoldPoints.getCombinedRestitution(id)
        set(value) { ManifoldPoints.setCombinedRestitution(id, value) }

    var contactMotionA: Float
        get() = ManifoldPoints.getContactMotion1(id)
        set(value) { ManifoldPoints.setContactMotion1(id, value) }
    var contactMotionB: Float
        get() = ManifoldPoints.getContactMotion2(id)
        set(value) { ManifoldPoints.setContactMotion2(id, value) }

    var distance: Float
        get() = ManifoldPoints.getDistance1(id)
        set(value) { ManifoldPoints.setDistance1(id, value) }
}
