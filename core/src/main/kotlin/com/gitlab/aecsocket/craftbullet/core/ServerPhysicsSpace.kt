package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bullet.PhysicsSpace
import com.jme3.bullet.collision.PhysicsCollisionObject
import com.jme3.bullet.collision.PhysicsRayTestResult
import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.collision.shapes.PlaneCollisionShape
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.math.Plane
import com.jme3.math.Vector3f
import com.simsilica.mathd.Vec3d
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet
import kotlin.math.sqrt

const val UPDATE_INTERVAL = 0.05f

// based on https://github.com/LazuriteMC/Rayon/blob/7c1597679bb777de2b660b93da52d68217b8139f/common/src/main/java/dev/lazurite/rayon/impl/bullet/collision/space/MinecraftSpace.java

data class BlockPos(val x: Int, val y: Int, val z: Int)

data class BlockData(
    val shape: CollisionShape,
    val type: Int,
    val friction: Float,
    val restitution: Float,
)

fun BlockData.applyTo(body: PhysicsRigidBody) {
    body.friction = friction
    body.restitution = restitution
}

interface TrackedPhysicsObject {
    val body: PhysicsCollisionObject
    val id: UUID

    fun preStep(space: ServerPhysicsSpace) {}

    fun postStep(space: ServerPhysicsSpace) {}
}

abstract class ServerPhysicsSpace(
    val bullet: CraftBulletAPI,
    val name: String,
    settings: Settings,
) : PhysicsSpace(BroadphaseType.DBVT) {
    @ConfigSerializable
    data class Settings(
        val updateInterval: Float = UPDATE_INTERVAL,
        val subSteps: Int = 4,
        val floorYLevel: Float = -128f,
        val blockFriction: Float = 2.0f,
        val blockRestitution: Float = 0.25f,
        val computeTerrain: Boolean = true,
        val computeBlocksAroundBodies: Double = 0.5,
    )

    private fun buildPlaneShape(): PlaneCollisionShape {
        return PlaneCollisionShape(Plane(Vector3f.UNIT_Y, settings.floorYLevel))
    }

    var settings = settings
        set(value) {
            field = value
            floor.collisionShape = buildPlaneShape()
        }
    private val physicsThread = bullet.physicsThread

    val floor = PhysicsRigidBody(buildPlaneShape(), STATIC_MASS).also {
        super.addCollisionObject(it)
    }

    private val _blocks = HashMap<BlockPos, BlockRigidBody>()
    val blocks: Map<BlockPos, BlockRigidBody> get() = _blocks

    private val _tracked = HashMap<UUID, TrackedPhysicsObject>()
    val tracked: Map<UUID, TrackedPhysicsObject> get() = _tracked

    init {
        setMaxSubSteps(settings.subSteps)
    }

    // so we can safely call in ctor
    final override fun setMaxSubSteps(steps: Int) {
        super.setMaxSubSteps(steps)
    }

    private fun assertThread() {
        physicsThread.assertCurrent()
    }

    // NB: make sure to remove object joints BEFORE removing the object
    // otherwise you get a segfault at btDiscreteDynamicsWorld::calculateSimulationIslands()
    override fun addCollisionObject(pco: PhysicsCollisionObject) {
        assertThread()
        if (pco is TrackedPhysicsObject) {
            _tracked[pco.id]?.let {
                bullet.log.warning("Added tracked object $pco (${pco.id}) which already exists as $it")
            }
            _tracked[pco.id] = pco
            super.addCollisionObject(pco.body)
        } else {
            super.addCollisionObject(pco)
        }
    }

    override fun removeCollisionObject(pco: PhysicsCollisionObject) {
        assertThread()
        if (pco is TrackedPhysicsObject) {
            _tracked.remove(pco.id)?.let {
                if (it !== pco)
                    bullet.log.warning("Tracked object $pco (${pco.id}) was removed from space $name, but its ID was linked to $it")
            } ?: bullet.log.warning("Tracked object $pco (${pco.id}) was removed from space $name, but was not initially present")
            super.removeCollisionObject(pco.body)
        } else {
            super.removeCollisionObject(pco)
        }
    }

    override fun needsCollision(pcoA: PhysicsCollisionObject, pcoB: PhysicsCollisionObject): Boolean {
        return bullet.doesContact(pcoA, pcoB, this)
    }

    override fun onContactProcessed(pcoA: PhysicsCollisionObject, pcoB: PhysicsCollisionObject, pointId: Long) {
        bullet.contact(pcoA, pcoB, ContactManifoldPoint(pointId), this)
    }

    fun update() {
        computeTerrain()

        val tracked = _tracked.toMap()
        tracked.forEach { (_, obj) ->
            obj.preStep(this)
        }

        update(settings.updateInterval, settings.subSteps, false, true, false)

        tracked.forEach { (_, obj) ->
            obj.postStep(this)
        }
    }

    fun blockBodyAt(pos: BlockPos) = blocks[pos]

    fun hasTracked(id: UUID) = _tracked.contains(id)

    fun trackedBy(id: UUID) = _tracked[id]

    fun removeTracked(id: UUID) {
        _tracked[id]?.let {
            removeCollisionObject(it.body)
        }
    }

    fun explode(origin: Vec3d, maxRadius: Double, power: Double) {
        assertThread()
        val maxRadiusSqr = maxRadius * maxRadius
        rigidBodyList.forEach { body ->
            if (body.isDynamic) {
                val bodyPos = body.physPosition
                val delta = bodyPos - origin
                val distanceSqr = delta.lengthSq()
                if (distanceSqr < maxRadiusSqr) {
                    val launchDir = delta.normalizeLocal()
                    val launchMag = (maxRadius - sqrt(distanceSqr)) * power
                    body.applyCentralImpulse((launchDir * launchMag).sp())
                }
            }
        }
    }

    protected abstract fun blockPositionsIn(box: BoundingBoxDp): Iterable<BlockPos>

    protected abstract fun blockDataAt(pos: BlockPos): BlockData

    private fun BlockData.addObject(pos: BlockPos): BlockRigidBody {
        val body = BlockRigidBody(shape, type, pos).also {
            applyTo(it)
            it.physPosition = Vec3d(pos.x.toDouble(), pos.y.toDouble(), pos.z.toDouble())
        }
        addCollisionObject(body)
        return body
    }

    fun rayTestWorld(from: Vec3d, to: Vec3d): List<PhysicsRayTestResult> {
        // code taken from NMS BlockGetter.clip implementation
        fun lerp(fac: Double, from: Double, to: Double) =
            from + fac * (to - from)

        fun lerp(fac: Double, from: Vec3d, to: Vec3d) = Vec3d(
            lerp(fac, from.x, to.x),
            lerp(fac, from.y, to.y),
            lerp(fac, from.z, to.z),
        )

        fun floor(value: Double): Int {
            val i = value.toInt()
            return if (value < i) i - 1 else i
        }

        fun sign(value: Double): Int {
            return if (value == 0.0) 0 else if (value > 0.0) 1 else -1
        }

        fun frac(value: Double): Double {
            return value - floor(value)
        }

        val (d0, d1, d2) = lerp(-1e-7, to, from)
        val (d3, d4, d5) = lerp(-1e-7, from, to)
        var x = floor(d3)
        var y = floor(d4)
        var z = floor(d5)

        val dx = d0 - d3
        val dy = d1 - d4
        val dz = d2 - d5
        val nx = sign(dx)
        val ny = sign(dy)
        val nz = sign(dz)
        val sx = if (nx == 0) Double.MAX_VALUE else nx.toDouble() / dx
        val sy = if (ny == 0) Double.MAX_VALUE else ny.toDouble() / dy
        val sz = if (nz == 0) Double.MAX_VALUE else nz.toDouble() / dz
        var ex = sx * (if (nx > 0) 1f - frac(d3) else frac(d4))
        var ey = sy * (if (ny > 0) 1f - frac(d4) else frac(d4))
        var ez = sz * (if (nz > 0) 1f - frac(d5) else frac(d5))

        while (ex < 1f || ey < 1f || ez < 1f) {
            if (ex < ey) {
                if (ex < ez) {
                    x += nx
                    ex += sx
                } else {
                    z += nz
                    ez += sz
                }
            } else if (ey < ez) {
                y += ny
                ey += sy
            } else {
                z += nz
                ez += sz
            }

            val pos = BlockPos(x, y, z)
            if (!_blocks.contains(pos)) {
                _blocks[pos] = blockDataAt(pos).addObject(pos)
            }
        }

        return rayTestDp(from, to, ArrayList())
    }

    private fun computeTerrain() {
        val processed = HashSet<BlockPos>()

        if (settings.computeTerrain) {
            rigidBodyList.forEach { body ->
                // for each active non-inbuilt body in our space...
                if (body.isActive && body !is InbuiltPhysicsObject) {
                    // we find what blocks are near it - which blocks it *might* collide with
                    val inflation = settings.computeBlocksAroundBodies

                    blockPositionsIn(body.boundingBox().inflated(inflation)).forEach { pos ->
                        // if we haven't processed this block yet, process it
                        if (!processed.contains(pos)) {
                            processed.add(pos)

                            val new = blockDataAt(pos)
                            _blocks[pos]?.let { old ->
                                // if we have a block for this pos left over...
                                if (old.type != new.type) {
                                    // if the block is different to the new, we replace it
                                    removeCollisionObject(old)
                                    _blocks[pos] = new.addObject(pos)
                                } // else we just keep the old block
                            } ?: run {
                                // if this is a new block, just add it
                                _blocks[pos] = new.addObject(pos)
                            }
                        }
                    }
                }
            }
        }

        // remove blocks we haven't processed
        val iter = _blocks.iterator()
        while (iter.hasNext()) {
            val (pos, body) = iter.next()
            if (!processed.contains(pos)) {
                removeCollisionObject(body)
                iter.remove()
            }
        }
    }
}
