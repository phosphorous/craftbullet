package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bullet.joints.New6Dof
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.system.JmeSystem
import com.jme3.system.Platform
import java.io.File
import java.net.URL
import java.nio.file.Files
import java.util.logging.Level

const val BUILD_TYPE = "Release"
const val FLAVOR = "Dp"
const val JME_VERSION = "17.4.0"

fun Platform.nativePathSuffix(): String {
    return when (this) {
        Platform.Windows32, Platform.Windows64 -> "bulletjme.dll"
        Platform.Linux_ARM32, Platform.Linux_ARM64, Platform.Linux32, Platform.Linux64 -> "libbulletjme.so"
        Platform.MacOSX32, Platform.MacOSX64, Platform.MacOSX_ARM64 -> "libbulletjme.dylib"
        else -> throw IllegalArgumentException("Platform $this has no native path suffix")
    }
}

object NativesLoader {
    fun load(api: CraftBulletAPI, root: File) {
        val platform = JmeSystem.getPlatform()
        // for downloading from Libbulletjme repo
        val remoteFileName = "$platform$BUILD_TYPE${FLAVOR}_${platform.nativePathSuffix()}"
        // for loading from pre-downloaded native
        val localFileName = "$platform$BUILD_TYPE${FLAVOR}_${JME_VERSION}_${platform.nativePathSuffix()}"

        val nativeFile = root.resolve(localFileName)

        if (!root.resolve(localFileName).exists()) {
            // auto-download
            val urlAddr = "https://github.com/stephengold/Libbulletjme/releases/download/$JME_VERSION/$remoteFileName"
            val url = URL(urlAddr)
            api.log.info("Downloading natives from $urlAddr to $localFileName")
            url.openStream().use { stream ->
                root.mkdirs()
                Files.copy(stream, nativeFile.toPath())
            }
            api.log.info("Downloaded")
        }

        try {
            System.load(nativeFile.absolutePath)
        } catch (ex: Exception) {
            api.log.severe("Could not load native library")
            ex.printStackTrace()
        }

        // avoid excess logging
        PhysicsRigidBody.logger2.level = Level.WARNING
        New6Dof.logger2.level = Level.WARNING
    }
}
