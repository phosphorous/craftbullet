package com.gitlab.aecsocket.craftbullet.core

class Timings(val expireAfter: Long) {
    private data class Entry(val timing: Long, val at: Long = System.currentTimeMillis())

    private val backing = ArrayList<Entry>()

    fun evict() {
        // any entries older than `threshold` get removed
        val threshold = System.currentTimeMillis() - expireAfter
        while (backing.isNotEmpty() && backing.first().at < threshold) {
            backing.removeFirst()
        }
    }

    val size: Int get() = backing.size

    fun push(element: Long) {
        evict()
        backing.add(Entry(element))
    }

    fun time(action: () -> Unit) {
        evict()
        val start = System.currentTimeMillis()
        action()
        val time = System.currentTimeMillis() - start
        backing.add(Entry(time))
    }

    fun clear() {
        backing.clear()
    }

    fun allEntries(): List<Long> {
        evict()
        return backing.map { it.timing }
    }

    fun lastEntries(upTo: Long) : List<Long> {
        evict()
        val threshold = System.currentTimeMillis() - upTo
        return backing.toList().filter { it.at >= threshold }.map { it.timing }
    }
}
