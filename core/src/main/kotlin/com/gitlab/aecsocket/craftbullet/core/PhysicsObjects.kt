package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.objects.PhysicsRigidBody
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.simsilica.mathd.Quatd
import com.simsilica.mathd.Vec3d

sealed interface InbuiltPhysicsObject

abstract class EntityPhysicsObject(
    shape: CollisionShape,
    mass: Float,
) : PhysicsRigidBody(shape, mass), InbuiltPhysicsObject, TrackedPhysicsObject {
    override val body get() = this

    protected abstract fun isValid(): Boolean

    protected abstract fun isPlayer(): Boolean

    protected abstract fun position(): Vec3d

    protected abstract fun rotation(): Quatd

    protected abstract fun velocity(): Vec3d

    override fun preStep(space: ServerPhysicsSpace) {
        if (!isValid()) {
            space.removeCollisionObject(body)
            return
        }

        if (isPlayer()) {
            activate()
        }

        if (isActive) {
            physPosition = position()
            physRotation = rotation()
            linearVelocity = velocity()
            angularVelocity = Vec3d.ZERO
        }
    }
}

class BlockRigidBody(
    shape: CollisionShape,
    val type: Int,
    val pos: BlockPos,
) : PhysicsRigidBody(shape, STATIC_MASS), InbuiltPhysicsObject
