package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bullet.collision.shapes.BoxCollisionShape
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape
import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.collision.shapes.CompoundCollisionShape
import com.jme3.bullet.collision.shapes.SphereCollisionShape
import com.jme3.math.Vector3f
import com.simsilica.mathd.Vec3d
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.max
import kotlin.math.sin

interface Drawable {
    fun draw(pos: Vec3d)
}

@ConfigSerializable
data class DrawSettings(
    val linePoints: Int = 20,
    val lineMinStep: Double = 0.1,
    val circlePoints: Int = 20,
)

data class BoxVertices(
    val b0: Vec3d, val b1: Vec3d, val b2: Vec3d, val b3: Vec3d,
    val t0: Vec3d, val t1: Vec3d, val t2: Vec3d, val t3: Vec3d,
) {
    fun map(map: (Vec3d) -> Vec3d) = BoxVertices(
        map(b0), map(b1), map(b2), map(b3),
        map(t0), map(t1), map(t2), map(t3),
    )
}

fun boxVerticesOf(min: Vec3d, max: Vec3d) = BoxVertices(
    min,                        Vec3d(max.x, min.y, min.z), Vec3d(max.x, min.y, max.z), Vec3d(min.x, min.y, max.z),
    Vec3d(min.x, max.y, min.z), Vec3d(max.x, max.y, min.z), max,                        Vec3d(min.x, max.y, max.z)
)

fun boxVerticesOf(halfExtents: Vec3d) = boxVerticesOf(-halfExtents, halfExtents)

fun drawPointsLine(
    from: Vec3d,
    to: Vec3d,
    settings: DrawSettings = DrawSettings()
): Array<Vec3d> {
    val delta = to - from
    val deltaLen = delta.length()
    val step = max(settings.lineMinStep, deltaLen / settings.linePoints)
    val stepVec = delta.normalizeLocal() * step
    val points = (deltaLen / step).toInt()

    return Array(points) { from + stepVec * it.toDouble() }
}

fun drawPointsBox(
    vertices: BoxVertices,
    settings: DrawSettings = DrawSettings()
): Array<Vec3d> {
    val (b0, b1, b2, b3, t0, t1, t2, t3) = vertices

    fun line(from: Vec3d, to: Vec3d): Array<Vec3d> {
        return drawPointsLine(from, to, settings)
    }

    return line(b0, b1) + line(b1, b2) + line(b2, b3) + line(b3, b0) +
            line(t0, t1) + line(t1, t2) + line(t2, t3) + line(t3, t0) +
            line(b0, t0) + line(b1, t1) + line(b2, t2) + line(b3, t3)
}

private enum class CapsuleAxis(val mapPoints: (Double, Double, Double) -> Vec3d) {
    X   ({ x, y, z -> Vec3d(y, x, z) }),
    Y   ({ x, y, z -> Vec3d(x, y, z) }),
    Z   ({ x, y, z -> Vec3d(x, z, y) })
}

fun drawPointsShape(shape: CollisionShape, settings: DrawSettings = DrawSettings()): Iterable<Vec3d> {
    fun map(value: Int, start1: Double, stop1: Double, start2: Double, stop2: Double): Double {
        val proportion = (value - start1) / (stop1 - start1)
        return start2 + proportion * (stop2 - start2)
    }

    return when (shape) {
        is BoxCollisionShape -> drawPointsBox(boxVerticesOf(shape.halfExtents), settings).asIterable()
        is SphereCollisionShape -> {
            val radius = shape.radius
            val total = settings.circlePoints + 1
            val fTotal = settings.circlePoints.toDouble()

            Array(total * total) { i ->
                val lat = map(i / total, 0.0, fTotal, 0.0, PI)
                val lon = map(i % total, 0.0, fTotal, 0.0, PI2)
                val x = radius * sin(lat) * cos(lon)
                val y = radius * cos(lat)
                val z = radius * sin(lat) * sin(lon)
                Vec3d(x, y, z)
            }.asIterable()
        }
        // todo this works good enough for now, but it can be cleaner
        is CapsuleCollisionShape -> {
            val radius = shape.radius.toDouble()
            val height = shape.height.toDouble()
            val axis = when (val axis = shape.axis) {
                0 -> CapsuleAxis.X
                1 -> CapsuleAxis.Y
                2 -> CapsuleAxis.Z
                else -> throw IllegalStateException("Unknown axis $axis")
            }

            val heightStep = max(settings.lineMinStep, height / settings.linePoints)
            val heightPoints = (height / heightStep).toInt()
            val halfHeight = height / 2

            val total = settings.circlePoints + 1
            val fTotal = settings.circlePoints.toDouble()

            val center = Array((heightPoints + 1) * total) {
                val curHeight = -halfHeight + heightStep * ((it+1) / total)
                val ang = (it % total) / heightPoints.toFloat() * FPI2
                val x = radius * cos(ang)
                val z = radius * sin(ang)
                axis.mapPoints(x, curHeight, z)
            }

            val halfTotal = total * total / 2
            val ends = Array(total * total) {
                val lat = map(it / total, 0.0, fTotal, 0.0, PI)
                val lon = map(it % total, 0.0, fTotal, 0.0, PI2)
                val x = radius * sin(lat) * cos(lon)
                val y = radius * cos(lat)
                val z = radius * sin(lat) * sin(lon)

                if (it >= halfTotal) Vec3d(x, y - halfHeight, z)
                else Vec3d(x, y + halfHeight, z)
            }

            (center + ends).asIterable()
        }
        is CompoundCollisionShape -> {
            shape.listChildren().flatMap { child ->
                val transform = child.transform
                drawPointsShape(child.shape).map { transform.transform(it) }
            }
        }
        else -> emptyList()
    }
}
