package com.gitlab.aecsocket.craftbullet.core

import com.jme3.bullet.collision.PhysicsCollisionObject
import java.util.UUID
import java.util.logging.Logger

data class DoesContactContext(
    val bodyA: PhysicsCollisionObject,
    val bodyB: PhysicsCollisionObject,
    val space: ServerPhysicsSpace
) {
    var doesContact: Boolean = true
}

typealias DoesContactListener = (DoesContactContext) -> Unit

data class ContactContext(
    val bodyA: PhysicsCollisionObject,
    val bodyB: PhysicsCollisionObject,
    val point: ContactManifoldPoint,
    val space: ServerPhysicsSpace
)

typealias ContactListener = (ContactContext) -> Unit

typealias StepListener = () -> Unit

interface CraftBulletAPI {
    val log: Logger
    val physicsThread: PhysicsThread
    val spaces: Map<UUID, ServerPhysicsSpace>

    fun executePhysics(task: Runnable) = physicsThread.execute(task)
    
    fun doesContact(
        bodyA: PhysicsCollisionObject,
        bodyB: PhysicsCollisionObject,
        space: ServerPhysicsSpace
    ): Boolean

    fun onDoesContact(listener: DoesContactListener)

    fun contact(
        bodyA: PhysicsCollisionObject,
        bodyB: PhysicsCollisionObject,
        point: ContactManifoldPoint,
        space: ServerPhysicsSpace
    )

    fun onContact(listener: ContactListener)

    fun onPreStep(listener: StepListener)

    fun onPostStep(listener: StepListener)
}
