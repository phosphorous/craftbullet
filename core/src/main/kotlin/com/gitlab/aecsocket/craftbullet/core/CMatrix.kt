package com.gitlab.aecsocket.craftbullet.core

// we need these wrapper classes for some rotation calculations
// LBJ doesn't provide get methods for the components of a Matrix3 (???)

class CMatrix3f(
    var m00: Float = 0f, var m01: Float = 0f, var m02: Float = 0f,
    var m10: Float = 0f, var m11: Float = 0f, var m12: Float = 0f,
    var m20: Float = 0f, var m21: Float = 0f, var m22: Float = 0f
)

class CMatrix3d(
    var m00: Double = 0.0, var m01: Double = 0.0, var m02: Double = 0.0,
    var m10: Double = 0.0, var m11: Double = 0.0, var m12: Double = 0.0,
    var m20: Double = 0.0, var m21: Double = 0.0, var m22: Double = 0.0
)
