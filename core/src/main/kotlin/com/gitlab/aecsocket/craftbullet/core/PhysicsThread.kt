package com.gitlab.aecsocket.craftbullet.core

import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Executor
import java.util.concurrent.atomic.AtomicBoolean

// based on https://github.com/LazuriteMC/Rayon/blob/main/common/src/main/java/dev/lazurite/rayon/impl/bullet/thread/PhysicsThread.java

class PhysicsThread(
    private val api: CraftBulletAPI,
    name: String
) : Thread(name), Executor {
    private val tasks = ConcurrentLinkedQueue<Runnable>()
    private val running = AtomicBoolean(true)

    fun assertCurrent() {
        val current = currentThread()
        if (current !== this)
            throw IllegalStateException("Must run physics operations on thread $name, ran on ${current.name}")
    }

    override fun run() {
        while (running.get()) {
            while (!tasks.isEmpty()) {
                try {
                    tasks.poll().run()
                } catch (ex: Exception) {
                    api.log.warning("Physics thread task threw an exception")
                    ex.printStackTrace()
                }
            }
        }
    }

    override fun execute(task: Runnable) {
        tasks.add(task)
    }

    fun destroy() {
        running.set(false)

        try {
            join(5000)
        } catch (ex: InterruptedException) {
            api.log.warning("Could not wait for physics thread to join")
            ex.printStackTrace()
        }
    }
}
