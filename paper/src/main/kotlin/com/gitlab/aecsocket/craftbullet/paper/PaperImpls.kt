package com.gitlab.aecsocket.craftbullet.paper

import com.gitlab.aecsocket.craftbullet.core.*
import com.jme3.bullet.collision.shapes.BoxCollisionShape
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape
import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.collision.shapes.EmptyShape
import net.minecraft.util.Mth
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.entity.Entity
import org.bukkit.entity.Player

internal val emptyShape = EmptyShape(false)

private val airData = BlockData(emptyShape, 0, 0f, 0f)

class PaperServerPhysicsSpace(
    api: CraftBulletAPI,
    settings: Settings,
    val world: World,
) : ServerPhysicsSpace(api, world.name, settings) {
    override fun blockPositionsIn(box: BoundingBoxDp): Iterable<BlockPos> {
        val min = box.min()
        val max = box.max()

        val sx = Mth.floor(min.x)
        val sy = Mth.floor(min.y)
        val sz = Mth.floor(min.z)

        val ex = Mth.floor(max.x)
        val ey = Mth.floor(max.y)
        val ez = Mth.floor(max.z)
        return net.minecraft.core.BlockPos.betweenClosed(sx, sy, sz, ex, ey, ez)
            .map { BlockPos(it.x, it.y, it.z) }
    }

    override fun blockDataAt(pos: BlockPos): BlockData {
        val location = Location(world, pos.x.toDouble(), pos.y.toDouble(), pos.z.toDouble())
        if (!location.isChunkLoaded) return airData

        val block = location.block
        if (block.type == Material.AIR) return airData

        return BlockData(
            block.collisionShape.createCollision(),
            block.type.key().hashCode(),
            (1 - block.type.slipperiness) * settings.blockFriction, settings.blockRestitution
        )
    }
}

open class PaperEntityPhysicsObject(
    shape: CollisionShape,
    mass: Float,
    val entity: Entity,
) : EntityPhysicsObject(shape, mass) {
    override val id get() = entity.uniqueId

    private val isPlayer = entity is Player

    override fun isValid() = entity.isValid

    override fun isPlayer() = isPlayer

    override fun position() = entity.centerOfMass.vec3d()

    override fun rotation() = entity.rotation

    override fun velocity() = entity.velocity.vec3d() * 20.0 // m/t to m/s

    companion object {
        fun from(craftBullet: CraftBullet, entity: Entity): PaperEntityPhysicsObject {
            val mass = craftBullet.getCustomMass(entity) ?: craftBullet.entityTypeMass(entity.type)

            val shape = if (entity is Player) {
                val diameter = entity.width
                CapsuleCollisionShape((diameter / 2).toFloat(), (entity.height - diameter).toFloat())
            } else {
                // todo capsule shapes and stuff
                val aabb = entity.boundingBox
                val extent = aabb.max.subtract(aabb.min)
                BoxCollisionShape(extent.multiply(0.5).vec3f())
            }

            return PaperEntityPhysicsObject(shape, mass, entity).also {
                it.physPosition = entity.centerOfMass.vec3d()
                it.isKinematic = true
            }
        }
    }
}
