package com.gitlab.aecsocket.craftbullet.paper

import cloud.commandframework.ArgumentDescription
import cloud.commandframework.arguments.standard.BooleanArgument
import cloud.commandframework.arguments.standard.DoubleArgument
import cloud.commandframework.arguments.standard.FloatArgument
import cloud.commandframework.arguments.standard.StringArgument
import cloud.commandframework.bukkit.CloudBukkitCapabilities
import cloud.commandframework.bukkit.arguments.selector.SingleEntitySelector
import cloud.commandframework.bukkit.parsers.selector.SingleEntitySelectorArgument
import cloud.commandframework.execution.CommandExecutionCoordinator
import cloud.commandframework.minecraft.extras.MinecraftHelp
import cloud.commandframework.paper.PaperCommandManager
import com.gitlab.aecsocket.craftbullet.core.EntityPhysicsObject
import com.gitlab.aecsocket.craftbullet.core.BlockRigidBody
import net.kyori.adventure.text.Component.text
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.format.TextDecoration
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.persistence.PersistentDataType

private val TIMING_INTERVALS = listOf(5, 30, 60)

private fun desc(content: String) = ArgumentDescription.of(content)

private fun perm(key: String) = "craftbullet.command.$key"

internal class CraftBulletCommand(
    private val plugin: CraftBullet
) {
    private val manager = PaperCommandManager(
        plugin,
        CommandExecutionCoordinator.simpleCoordinator(),
        { it }, { it })

    private val root = manager.commandBuilder("craftbullet", desc("Core command plugin."))

    init {
        if (manager.hasCapability(CloudBukkitCapabilities.BRIGADIER))
            manager.registerBrigadier()

        val help = MinecraftHelp("/craftbullet help", { it }, manager)

        manager.command(root
            .literal("help", desc("Lists help information."))
            .argument(StringArgument.optional("query", StringArgument.StringMode.GREEDY))
            .handler { ctx ->
                val query = ctx.getOptional<String>("query").orElse("")
                help.queryCommands(
                    if (query.startsWith("craftbullet ")) query else "craftbullet $query",
                    ctx.sender
                )
            })
        manager.command(root
            .literal("reload", desc("Reloads all plugin data."))
            .permission(perm("reload"))
            .handler { ctx ->
                val sender = ctx.sender
                plugin.load()
                sender.sendMessage(text("Reloaded CraftBullet"))
            })
        manager.command(root
            .literal("stats", desc("Display statistics on the current state of the system."))
            .permission(perm("stats"))
            .handler { ctx ->
                val sender = ctx.sender
                sender.sendMessage(text("Objects per world:"))
                plugin.spaces.forEach { (worldId, space) ->
                    val world = Bukkit.getWorld(worldId)
                    val bodies = space.rigidBodyList
                    val entities = bodies.filterIsInstance<EntityPhysicsObject>()
                    val blocks = bodies.filterIsInstance<BlockRigidBody>()
                    sender.sendMessage(text(" · ${world?.name}: ${bodies.count { it.isActive }} active / ${bodies.size} bodies"))
                    sender.sendMessage(text("     " +
                        "Entities: ${entities.count { it.isActive }}/${entities.size}  " +
                        "Blocks: ${blocks.count { it.isActive }}/${blocks.size}"
                    ))
                }

                val updateTimings = plugin.updateTimings
                sender.sendMessage(text("Update timings:"))
                fun Double.format() = "%.1f".format(this)
                sender.sendMessage(text(" · Last: ${updateTimings.allEntries().lastOrNull() ?: "(none)"}ms"))
                sender.sendMessage(text(" · Mean of last:"))
                TIMING_INTERVALS.forEach { interval ->
                    sender.sendMessage(text("   ${interval}s: ${updateTimings.lastEntries(interval * 1000L).average().format()}ms"))
                }
            })
        manager.command(root
            .literal("mass", desc("Accesses the mass of an entity."))
            .argument(SingleEntitySelectorArgument.of("target"), desc("Entity to apply to."))
            .argument(FloatArgument.optional("mass"), desc("New mass to set to the entity."))
            .permission(perm("mass"))
            .handler { ctx ->
                val sender = ctx.sender
                val target = ctx.get<SingleEntitySelector>("target").entity
                val oMass = ctx.getOptional<Float>("mass")

                if (target == null) {
                    sender.sendMessage(text("No target selected", NamedTextColor.RED))
                } else {
                    oMass.ifPresentOrElse({ mass ->
                        if (mass <= 0) {
                            target.customMass = null
                            sender.sendMessage(text("Removed custom mass from ").append(target.name()))
                        } else {
                            target.customMass = mass
                            sender.sendMessage(text("Set custom mass of ").append(target.name()).append(text(" to $mass")))
                        }
                    }, {
                        val defaultMass = plugin.entityTypeMass(target.type)
                        sender.sendMessage(text("Custom mass of ").append(target.name()).append(text(": ${target.customMass ?: "(none)"}, default mass: $defaultMass")))
                    })
                }
            })
        manager.command(root
            .literal("collision", desc("Accesses the collision state of an entity."))
            .argument(SingleEntitySelectorArgument.of("target"), desc("Entity to apply to."))
            .argument(BooleanArgument.optional("collision"), desc("New collision state to set to the entity."))
            .permission(perm("collision"))
            .handler { ctx ->
                val sender = ctx.sender
                val target = ctx.get<SingleEntitySelector>("target").entity
                val oCollision = ctx.getOptional<Boolean>("collision")

                if (target == null) {
                    sender.sendMessage(text("No target selected", NamedTextColor.RED))
                } else {
                    oCollision.ifPresentOrElse({ collision ->
                        target.hasCollision = collision
                        sender.sendMessage(text("Set collision state of ").append(target.name()).append(text(" to $collision")))
                    }, {
                        sender.sendMessage(text("Collision state of ").append(target.name()).append(text(": ${target.hasCollision}")))
                    })
                }
            })
        manager.command(root
            .literal("draw", desc("Toggles drawing debug frames of bodies."))
            .flag(manager.flagBuilder("com")
                .withDescription(desc("Draw center of mass"))
                .withAliases("c"))
            .flag(manager.flagBuilder("velocity")
                .withDescription(desc("Draw velocity"))
                .withAliases("v"))
            .flag(manager.flagBuilder("shape")
                .withDescription(desc("Draw collision shape"))
                .withAliases("s"))
            .permission(perm("draw"))
            .senderType(Player::class.java)
            .handler { ctx ->
                val player = ctx.sender as Player
                plugin.drawSettings[player] = CraftBullet.DebugDrawSettings(
                    ctx.flags().hasFlag("com"),
                    ctx.flags().hasFlag("velocity"),
                    ctx.flags().hasFlag("shape"),
                )
            })
        manager.command(root
            .literal("launcher", desc("Gives a debug cube launcher."))
            .flag(manager.flagBuilder("force")
                .withDescription(desc("Force, in Newtons, to launch the body forwards."))
                .withAliases("f")
                .withArgument(DoubleArgument.builder<CommandSender>("force")
                    .withMin(0)))
            .flag(manager.flagBuilder("mass")
                .withDescription(desc("Mass, in kilograms, of the body."))
                .withAliases("m")
                .withArgument(FloatArgument.builder<CommandSender>("mass")
                    .withMin(0)))
            .flag(manager.flagBuilder("friction")
                .withDescription(desc("Friction of the body."))
                .withAliases("r")
                .withArgument(FloatArgument.builder<CommandSender>("friction")
                    .withMin(0)))
            .flag(manager.flagBuilder("collides")
                .withDescription(desc("If the box should collider with the launcher player."))
                .withAliases("c"))
            .permission(perm("launcher"))
            .senderType(Player::class.java)
            .handler { ctx ->
                val player = ctx.sender as Player
                val force = ctx.flags().get<Double>("force") ?: 0.0
                val mass = ctx.flags().get<Float>("mass") ?: 1f
                val friction = ctx.flags().get<Float>("friction") ?: 0.75f
                val collides = ctx.flags().hasFlag("collides")

                val item = plugin.settings.launcherItem.createItem()
                item.editMeta { meta ->
                    val pdc = meta.persistentDataContainer
                    pdc.set(plugin.keyLauncher, PersistentDataType.TAG_CONTAINER, pdc.adapterContext.newPersistentDataContainer().apply {
                        set(plugin.keyForce, PersistentDataType.DOUBLE, force)
                        set(plugin.keyMass, PersistentDataType.FLOAT, mass)
                        set(plugin.keyFriction, PersistentDataType.FLOAT, friction)
                        set(plugin.keyCollides, PersistentDataType.BYTE, if (collides) 1 else 0)
                    })

                    fun itText(text: String, color: TextColor) = text(text, color).decoration(TextDecoration.ITALIC, false)

                    meta.displayName(itText("Debug Cube Launcher", NamedTextColor.YELLOW))
                    meta.lore(listOf(
                        itText("Force: $force N", NamedTextColor.GRAY),
                        itText("Mass: $mass kg", NamedTextColor.GRAY),
                        itText("Friction: $friction", NamedTextColor.GRAY),
                        itText("Collides with player: $collides", NamedTextColor.GRAY)
                    ))
                }
                player.inventory.addItem(item)
            })
        manager.command(root
            .literal("remove-cubes", desc("Removes all debug cubes."))
            .permission(perm("remove-cubes"))
            .senderType(Player::class.java)
            .handler { ctx ->
                val player = ctx.sender as Player

                player.world.entities.forEach { entity ->
                    if (entity.persistentDataContainer.getOrDefault(plugin.keyDebugCube, PersistentDataType.BYTE, 0) == 1.toByte())
                        entity.remove()
                }
            })
    }
}
