package com.gitlab.aecsocket.craftbullet.paper

import net.kyori.adventure.key.Key
import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.Registry
import org.spongepowered.configurate.ConfigurationNode
import org.spongepowered.configurate.kotlin.extensions.get
import org.spongepowered.configurate.serialize.SerializationException
import org.spongepowered.configurate.serialize.TypeSerializer
import java.lang.reflect.Type

object MaterialSerializer : TypeSerializer<Material> {
    override fun serialize(type: Type, obj: Material?, node: ConfigurationNode) {
        if (obj == null) node.set(null)
        else {
            node.set(obj.key)
        }
    }

    override fun deserialize(type: Type, node: ConfigurationNode): Material {
        val key = node.get<Key>()
            ?: throw SerializationException(node, type, "Material must be expressed as namespaced key")
        return Registry.MATERIAL[NamespacedKey(key.namespace(), key.value())]
            ?: throw SerializationException(node, type, "Invalid Material $key")
    }
}
