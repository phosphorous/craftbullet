package com.gitlab.aecsocket.craftbullet.paper

import com.destroystokyo.paper.event.entity.EntityAddToWorldEvent
import com.destroystokyo.paper.event.server.ServerTickStartEvent
import com.gitlab.aecsocket.craftbullet.core.*
import com.jme3.bullet.collision.PhysicsCollisionObject
import com.jme3.bullet.collision.PhysicsRayTestResult
import com.jme3.bullet.collision.shapes.BoxCollisionShape
import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.collision.shapes.SphereCollisionShape
import com.jme3.bullet.objects.PhysicsGhostObject
import com.jme3.bullet.objects.PhysicsRigidBody
import com.simsilica.mathd.Vec3d
import net.kyori.adventure.serializer.configurate4.ConfigurateComponentSerializer
import org.bukkit.*
import org.bukkit.Particle.DustOptions
import org.bukkit.block.data.BlockData
import org.bukkit.entity.ArmorStand
import org.bukkit.entity.Entity
import org.bukkit.entity.EntityType
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockExplodeEvent
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.event.entity.EntityExplodeEvent
import org.bukkit.event.entity.EntityTeleportEvent
import org.bukkit.event.player.PlayerGameModeChangeEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.event.world.WorldUnloadEvent
import org.bukkit.inventory.EquipmentSlot
import org.bukkit.persistence.PersistentDataType
import org.bukkit.plugin.java.JavaPlugin
import org.spongepowered.configurate.ConfigurationOptions
import org.spongepowered.configurate.hocon.HoconConfigurationLoader
import org.spongepowered.configurate.kotlin.dataClassFieldDiscoverer
import org.spongepowered.configurate.kotlin.extensions.get
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import org.spongepowered.configurate.objectmapping.ObjectMapper
import org.spongepowered.configurate.util.NamingSchemes
import java.util.UUID

private lateinit var instance: CraftBullet
val CraftBulletAPI get() = instance

private const val PATH_SETTINGS = "settings.conf"

private val configOptions = ConfigurationOptions.defaults()
    .serializers {
        val mapper = ObjectMapper.factoryBuilder()
            .addDiscoverer(dataClassFieldDiscoverer())
            .defaultNamingScheme(NamingSchemes.SNAKE_CASE)
        it.registerAll(ConfigurateComponentSerializer.configurate().serializers())
        it.register(Material::class.java, MaterialSerializer)
        it.register(Particle::class.java, ParticleSerializer)
        it.register(Vec3d::class.java, Vec3dSerializer)
        it.register(Color::class.java, ColorSerializer)
        it.register(BlockData::class.java, BlockDataSerializer)
        it.register(DustOptions::class.java, DustOptionsSerializer)
        it.register(BulletParticleEffect::class.java, ParticleEffectSerializer)
        it.register(EntityType::class.java, EntityTypeSerializer)
        it.registerAnnotatedObjects(mapper.build())
    }

class CraftBullet : JavaPlugin(), CraftBulletAPI {
    companion object {
        @JvmStatic
        fun instance() = CraftBulletAPI
    }

    @ConfigSerializable
    data class Settings(
        val space: ServerPhysicsSpace.Settings = ServerPhysicsSpace.Settings(),

        val explosionMaxRadius: Double = 0.0,
        val explosionPower: Double = 0.0,

        val entityCollisionWhitelist: Set<EntityType> = emptySet(),
        val entityCollisionBlacklist: Set<EntityType> = emptySet(),
        val entityMass: Map<EntityType, Float> = emptyMap(),
        val defaultEntityMass: Float = 1f,

        val launcherItem: ItemDescriptor = ItemDescriptor(Material.STICK),
        val launcherModel: ItemDescriptor = ItemDescriptor(Material.STONE),
        val launcherModelSmall: Boolean = false,

        val drawRadius: Float = 16f,
        val drawSettings: DrawSettings = DrawSettings(),
        val drawVelocityMultiplier: Double = 0.2,
        val drawParticles: Map<DrawType, BulletParticleEffect> = emptyMap(),
    )

    data class DebugDrawSettings(
        val centerOfMass: Boolean,
        val velocity: Boolean,
        val shape: Boolean,
    )

    enum class DrawType {
        POINT, VECTOR, SHAPE
    }

    private val keyNoCollision = NamespacedKey(this, "no_collision")
    private val keyCustomMass = NamespacedKey(this, "custom_mass")

    internal val keyLauncher = NamespacedKey(this, "launcher")
    internal val keyForce = NamespacedKey(this, "force")
    internal val keyMass = NamespacedKey(this, "mass")
    internal val keyFriction = NamespacedKey(this, "friction")
    internal val keyCollides = NamespacedKey(this, "collides")
    internal val keyDebugCube = NamespacedKey(this, "debug_cube")

    override val log get() = logger

    override val physicsThread = PhysicsThread(this, "Physics thread")

    lateinit var settings: Settings private set

    var updating = false
        private set

    val updateTimings = Timings(60 * 1000)

    private val _spaces = HashMap<UUID, ServerPhysicsSpace>()
    override val spaces: Map<UUID, ServerPhysicsSpace> get() = _spaces

    internal val drawSettings = HashMap<Player, DebugDrawSettings>()
    private var drawOperations: List<Runnable> = emptyList()

    private val onDoesContact = ArrayList<DoesContactListener>()
    private val onContact = ArrayList<ContactListener>()
    private val onPreStep = ArrayList<StepListener>()
    private val onPostStep = ArrayList<StepListener>()

    init {
        instance = this
    }

    override fun onLoad() {
        setupResources()
        NativesLoader.load(this, dataFolder)
    }

    override fun onEnable() {
        CraftBulletCommand(this)
        load()

        physicsThread.start()

        Bukkit.getPluginManager().registerEvents(object : Listener {
            @EventHandler
            fun on(event: PlayerQuitEvent) {
                drawSettings.remove(event.player)
            }

            @EventHandler
            fun on(event: ServerTickStartEvent) {
                executePhysics {
                    if (updating) return@executePhysics
                    updating = true

                    updateTimings.time {
                        val drawOperations = ArrayList<Runnable>()

                        fun forPlayer(player: Player, space: ServerPhysicsSpace) {
                            val drawSettings = drawSettings[player] ?: return
                            val drawPoint = drawableOf(player, DrawType.POINT)
                            val drawVector = drawableOf(player, DrawType.VECTOR)
                            val drawShape = drawableOf(player, DrawType.SHAPE)

                            val drawGhost = PhysicsGhostObject(SphereCollisionShape(settings.drawRadius))
                            val drawBodies = drawGhost.apply {
                                physPosition = player.location.vec3d()
                                space.addCollisionObject(this)
                            }.overlappingObjects
                            space.removeCollisionObject(drawGhost)

                            drawBodies.forEach { body ->
                                if (body is BlockRigidBody) return@forEach
                                val com = body.physPosition

                                if (drawSettings.centerOfMass) {
                                    drawOperations.add {
                                        drawPoint.draw(com)
                                    }
                                }

                                if (body !is PhysicsRigidBody) return@forEach

                                if (drawSettings.velocity) {
                                    val velocity = body.linearVelocity
                                    val points = drawPointsLine(
                                        com,
                                        com + velocity * settings.drawVelocityMultiplier,
                                        settings.drawSettings
                                    )

                                    drawOperations.add {
                                        points.forEach { drawVector.draw(it) }
                                    }
                                }

                                if (drawSettings.shape) {
                                    val points = drawPointsShape(body.collisionShape)
                                        .map { body.transform.transform(it) }

                                    drawOperations.add {
                                        points.forEach { drawShape.draw(it) }
                                    }
                                }
                            }
                        }

                        preStep()

                        _spaces.toMap().forEach { (worldId, space) ->
                            Bukkit.getWorld(worldId)?.let { world ->
                                space.update()

                                world.players.forEach { forPlayer(it, space) }
                            } ?: run {
                                space.destroy()
                                _spaces.remove(worldId)
                            }
                        }

                        postStep()

                        this@CraftBullet.drawOperations = drawOperations
                        updating = false
                    }
                }
            }

            @EventHandler
            fun on(event: EntityAddToWorldEvent) {
                val entity = event.entity
                if (!entity.hasCollision) return

                val space = spaceOf(entity.world)
                executePhysics {
                    space.addCollisionObject(PaperEntityPhysicsObject.from(this@CraftBullet, entity))
                }
            }

            @EventHandler
            fun on(event: EntityExplodeEvent) {
                val location = event.location
                val entity = event.entity
                executePhysics {
                    _spaces[location.world.uid]?.explode(entity.centerOfMass.vec3d(), settings.explosionMaxRadius, settings.explosionPower)
                }
            }

            @EventHandler
            fun on(event: BlockExplodeEvent) {
                val block = event.block
                executePhysics {
                    _spaces[block.world.uid]?.explode(block.location.toCenterLocation().vec3d(), settings.explosionMaxRadius, settings.explosionPower)
                }
            }

            @EventHandler
            fun on(event: EntityTeleportEvent) {
                val entity = event.entity
                val from = event.from
                if (!entity.hasCollision) return

                event.to?.let { to ->
                    if (from.world !== to.world) {
                        val id = entity.uniqueId
                        val fromSpace = spaceOf(from.world)
                        val toSpace = spaceOf(to.world)

                        fromSpace.tracked[id]?.let { tracked ->
                            fromSpace.removeCollisionObject(tracked.body)
                            toSpace.addCollisionObject(tracked.body)
                        }
                    }
                }
            }

            @EventHandler
            fun on(event: PlayerGameModeChangeEvent) {
                val player = event.player

                scheduleDelayed {
                    player.updateHasCollision()
                }
            }

            @EventHandler
            fun on(event: WorldUnloadEvent) {
                removeSpace(event.world.uid)
            }

            @EventHandler
            fun on(event: PlayerInteractEvent) {
                if (event.hand != EquipmentSlot.HAND) return
                event.item?.let { item ->
                    item.itemMeta.persistentDataContainer.get(keyLauncher, PersistentDataType.TAG_CONTAINER)?.let { launcher ->
                        event.isCancelled = true
                        val force = launcher.getOrDefault(keyForce, PersistentDataType.DOUBLE, 0.0)
                        val mass = launcher.getOrDefault(keyMass, PersistentDataType.FLOAT, 1f)
                        val friction = launcher.getOrDefault(keyFriction, PersistentDataType.FLOAT, 0.75f)
                        val collides = launcher.getOrDefault(keyCollides, PersistentDataType.BYTE, 0) == 1.toByte()
                        spawnDebugBox(event.player, force, mass, friction, collides)
                    }
                }
            }
        }, this)

        scheduleRepeating {
            drawOperations.forEach { it.run() }
        }
    }

    override fun onDisable() {
        physicsThread.destroy()
    }

    private fun setupResources() {
        if (!dataFolder.exists()) {
            saveResource(PATH_SETTINGS, false)
        }
    }

    fun load() {
        settings = try {
            HoconConfigurationLoader.builder()
                .defaultOptions(configOptions)
                .file(dataFolder.resolve(PATH_SETTINGS))
                .build().load()
                .get { Settings() }
        } catch (ex: Exception) {
            log.severe("Could not load config")
            ex.printStackTrace()
            Settings()
        }

        _spaces.forEach { (_, space) -> space.settings = settings.space }
    }

    override fun doesContact(
        bodyA: PhysicsCollisionObject,
        bodyB: PhysicsCollisionObject,
        space: ServerPhysicsSpace
    ): Boolean {
        val ctx = DoesContactContext(bodyA, bodyB, space)
        onDoesContact.forEach { it(ctx) }
        return ctx.doesContact
    }

    override fun onDoesContact(listener: DoesContactListener) {
        onDoesContact.add(listener)
    }

    override fun contact(
        bodyA: PhysicsCollisionObject,
        bodyB: PhysicsCollisionObject,
        point: ContactManifoldPoint,
        space: ServerPhysicsSpace
    ) {
        val ctx = ContactContext(bodyA, bodyB, point, space)
        onContact.forEach { it(ctx) }
    }

    override fun onContact(listener: ContactListener) {
        onContact.add(listener)
    }

    override fun onPreStep(listener: StepListener) {
        onPreStep.add(listener)
    }

    private fun preStep() {
        onPreStep.forEach { it() }
    }

    override fun onPostStep(listener: StepListener) {
        onPostStep.add(listener)
    }

    private fun postStep() {
        onPostStep.forEach { it() }
    }

    fun spaceOf(world: World): ServerPhysicsSpace {
        return _spaces.computeIfAbsent(world.uid) { PaperServerPhysicsSpace(this, settings.space, world) }
    }

    fun removeSpace(id: UUID) {
        _spaces.remove(id)?.destroy()
    }

    fun drawableOf(player: Player, drawType: DrawType): Drawable {
        val particle = settings.drawParticles[drawType]
        return particle?.let {
            object : Drawable {
                override fun draw(pos: Vec3d) {
                    particle.spawn(player, pos)
                }
            }
        } ?: object : Drawable {
            override fun draw(pos: Vec3d) {}
        }
    }

    fun drawPointsShape(shape: CollisionShape): Iterable<Vec3d> {
        return drawPointsShape(shape, settings.drawSettings)
    }

    fun entityTypeHasCollision(entityType: EntityType) =
        (
            settings.entityCollisionWhitelist.isEmpty()
            || settings.entityCollisionWhitelist.contains(entityType)
        ) && !settings.entityCollisionBlacklist.contains(entityType)

    fun defaultHasCollision(entity: Entity) =
        entityTypeHasCollision(entity.type)
        && (entity !is Player || entity.gameMode != GameMode.SPECTATOR)

    fun hasCollision(entity: Entity) =
        defaultHasCollision(entity)
        && entity.persistentDataContainer.get(keyNoCollision, PersistentDataType.BYTE) != 1.toByte()

    fun setCollision(entity: Entity, value: Boolean) {
        val pdc = entity.persistentDataContainer
        val space = spaceOf(entity.world)
        val id = entity.uniqueId
        if (value) {
            pdc.remove(keyNoCollision)
            executePhysics {
                if (!space.hasTracked(id)) {
                    space.addCollisionObject(PaperEntityPhysicsObject.from(this, entity))
                }
            }
        } else {
            pdc.set(keyNoCollision, PersistentDataType.BYTE, 1)
            executePhysics {
                space.removeTracked(id)
            }
        }
    }

    fun entityTypeMass(entityType: EntityType) =
        settings.entityMass[entityType] ?: settings.defaultEntityMass

    fun hasCustomMass(entity: Entity) =
        entity.persistentDataContainer.has(keyCustomMass, PersistentDataType.FLOAT)

    fun getCustomMass(entity: Entity) =
        entity.persistentDataContainer.get(keyCustomMass, PersistentDataType.FLOAT)

    fun setCustomMass(entity: Entity, value: Float) {
        entity.persistentDataContainer.set(keyCustomMass, PersistentDataType.FLOAT, value)
        entity.executePhysics { body ->
            body.mass = value
        }
    }

    fun removeCustomMass(entity: Entity) {
        entity.persistentDataContainer.remove(keyCustomMass)
        entity.executePhysics { body ->
            body.mass = entityTypeMass(entity.type)
        }
    }

    private fun spawnDebugBox(player: Player, force: Double, mass: Float, friction: Float, collides: Boolean) {
        val location = player.eyeLocation
        val impulse = location.direction.vec3d() * force

        val world = location.world
        val space = spaceOf(world)

        fun Location.correct() {
            subtract(0.0, 1.45, 0.0)
            yaw = 0f
            pitch = 0f
        }

        val spawnLoc = location.clone()
        spawnLoc.correct()
        val stand = world.spawnEntity(
            spawnLoc,
            EntityType.ARMOR_STAND,
            CreatureSpawnEvent.SpawnReason.CUSTOM
        ) { stand ->
            stand as ArmorStand
            stand.hasCollision = false
            stand.isSilent = true
            stand.isVisible = false
            stand.isMarker = true
            stand.isPersistent = false
            stand.setAI(false)
            stand.setGravity(false)
            stand.equipment.helmet = settings.launcherModel.createItem()
            stand.persistentDataContainer.set(keyDebugCube, PersistentDataType.BYTE, 1)
            stand.isSmall = settings.launcherModelSmall
        }

        val id = UUID.randomUUID()
        val body = object : PhysicsRigidBody(BoxCollisionShape(0.5f), mass), TrackedPhysicsObject {
            override val id get() = id
            override val body get() = this

            override fun preStep(space: ServerPhysicsSpace) {
                if (!stand.isValid) {
                    space.removeCollisionObject(body)
                    return
                }

                val entityLoc = body.motionState.position.location(world)
                entityLoc.correct()

                val rotation = body.motionState.rotation
                val pose = rotation.euler().pose()

                scheduleDelayed {
                    stand.teleportAsync(entityLoc)
                    (stand as ArmorStand).headPose = pose
                }
            }
        }.also {
            it.friction = friction
            it.physPosition = location.vec3d()
            it.applyCentralImpulse(impulse.sp())

            if (!collides) {
                player.collisionBody?.let { pBody -> it.addToIgnoreList(pBody) }
            }
        }

        executePhysics {
            space.add(body)
        }
    }
}

val Entity.collisionBody: PaperEntityPhysicsObject?
    get() {
        return CraftBulletAPI.spaceOf(world).trackedBy(uniqueId)?.let { body ->
            body as? PaperEntityPhysicsObject
                ?: throw IllegalStateException("Body for entity $this is not a ${PaperEntityPhysicsObject::class}")
            return body
        }
    }

fun Entity.executePhysics(task: (PaperEntityPhysicsObject) -> Unit) {
    CraftBulletAPI.executePhysics {
        collisionBody?.let { task(it) }
    }
}

var Entity.hasCollision: Boolean
    get() = CraftBulletAPI.hasCollision(this)
    set(value) { CraftBulletAPI.setCollision(this, value) }

fun Entity.updateHasCollision() {
    hasCollision = CraftBulletAPI.defaultHasCollision(this)
}

var Entity.customMass: Float?
    get() = CraftBulletAPI.getCustomMass(this)
    set(value) {
        if (value == null) CraftBulletAPI.removeCustomMass(this)
        else CraftBulletAPI.setCustomMass(this, value)
    }

var Entity.mass: Float
    get() = customMass ?: CraftBulletAPI.entityTypeMass(type)
    set(value) { CraftBulletAPI.setCustomMass(this, value) }

fun LivingEntity.rayTestFrom(distance: Double): List<PhysicsRayTestResult> {
    val location = eyeLocation
    val from = location.vec3d()
    val direction = location.direction.vec3d()
    val to = from + direction * distance

    val id = uniqueId
    return CraftBulletAPI.spaceOf(world).rayTestWorld(from, to)
        .filter {
            val obj = it.collisionObject
            obj !is EntityPhysicsObject || obj.id != id
        }
}
