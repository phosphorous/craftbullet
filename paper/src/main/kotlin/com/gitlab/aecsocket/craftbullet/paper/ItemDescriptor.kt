package com.gitlab.aecsocket.craftbullet.paper

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.Damageable
import org.spongepowered.configurate.objectmapping.ConfigSerializable
import org.spongepowered.configurate.objectmapping.meta.Required

@ConfigSerializable
data class ItemDescriptor(
    @Required val material: Material,
    val modelData: Int = 0,
    val damage: Int = 0
) {
    fun createItem(): ItemStack {
        val item = ItemStack(material)
        item.editMeta { meta ->
            meta.setCustomModelData(modelData)
            if (meta is Damageable) meta.damage = damage
        }
        return item
    }
}
