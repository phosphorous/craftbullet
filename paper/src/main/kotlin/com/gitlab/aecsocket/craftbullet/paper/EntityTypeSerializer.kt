package com.gitlab.aecsocket.craftbullet.paper

import net.kyori.adventure.key.Key
import org.bukkit.NamespacedKey
import org.bukkit.Registry
import org.bukkit.entity.EntityType
import org.spongepowered.configurate.ConfigurationNode
import org.spongepowered.configurate.kotlin.extensions.get
import org.spongepowered.configurate.serialize.SerializationException
import org.spongepowered.configurate.serialize.TypeSerializer
import java.lang.reflect.Type

object EntityTypeSerializer : TypeSerializer<EntityType> {
    override fun serialize(type: Type, obj: EntityType?, node: ConfigurationNode) {
        if (obj == null) node.set(null)
        else {
            node.set(obj.key)
        }
    }

    override fun deserialize(type: Type, node: ConfigurationNode): EntityType {
        val key = node.get<Key>()
            ?: throw SerializationException(node, type, "Entity type must be expressed as namespaced key")
        return Registry.ENTITY_TYPE[NamespacedKey(key.namespace(), key.value())]
            ?: throw SerializationException(node, type, "Invalid entity type $key")
    }
}
