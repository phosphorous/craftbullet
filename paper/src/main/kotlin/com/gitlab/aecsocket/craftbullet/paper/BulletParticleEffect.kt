package com.gitlab.aecsocket.craftbullet.paper

import com.simsilica.mathd.Vec3d
import io.leangen.geantyref.TypeToken
import net.kyori.adventure.key.Key
import net.kyori.adventure.text.format.TextColor
import net.minecraft.core.registries.BuiltInRegistries
import net.minecraft.core.registries.Registries
import net.minecraft.resources.ResourceKey
import net.minecraft.resources.ResourceLocation
import org.bukkit.Bukkit
import org.bukkit.Color
import org.bukkit.Particle
import org.bukkit.Particle.DustOptions
import org.bukkit.block.data.BlockData
import org.bukkit.craftbukkit.v1_19_R2.CraftParticle
import org.bukkit.entity.Player
import org.spongepowered.configurate.ConfigurationNode
import org.spongepowered.configurate.kotlin.extensions.get
import org.spongepowered.configurate.serialize.SerializationException
import org.spongepowered.configurate.serialize.TypeSerializer
import java.lang.reflect.Type

data class BulletParticleEffect(
    val particle: Particle,
    val count: Int = 0,
    val size: Vec3d = Vec3d.ZERO,
    val speed: Double = 0.0,
    val data: Any? = null
) {
    fun spawn(player: Player, position: Vec3d) {
        player.spawnParticle(particle,
            position.x, position.y, position.z, count,
            size.x, size.y, size.z, speed, data)
    }
}

private inline fun <reified T> typeToken() = object : TypeToken<T>() {}

private fun <V> ConfigurationNode.force(type: TypeToken<V>) = get(type)
    ?: throw SerializationException(this, type.type, "A value is required for this field")

private inline fun <reified V> ConfigurationNode.force() = force(typeToken<V>())

internal object ParticleSerializer : TypeSerializer<Particle> {
    override fun serialize(type: Type, obj: Particle?, node: ConfigurationNode) {}

    override fun deserialize(type: Type, node: ConfigurationNode): Particle {
        val key = node.get<Key>()
            ?: throw SerializationException(node, type, "Particle must be expressed as namespaced key")
        val resourceKey = ResourceKey.create(Registries.PARTICLE_TYPE, ResourceLocation(key.namespace(), key.value()))
        val nmsParticle = BuiltInRegistries.PARTICLE_TYPE[resourceKey]
            ?: throw SerializationException("Invalid Particle $key")
        return CraftParticle.toBukkit(nmsParticle)
    }
}

internal object Vec3dSerializer : TypeSerializer<Vec3d> {
    override fun serialize(type: Type, obj: Vec3d?, node: ConfigurationNode) {}

    override fun deserialize(type: Type, node: ConfigurationNode): Vec3d {
        return if (node.isList) {
            val list = node.childrenList()
            if (list.size != 3)
                throw SerializationException(node, type, "Vector must be defined as list of 3 components")
            Vec3d(list[0].force(), list[1].force(), list[2].force())
        } else {
            val n = node.force<Double>()
            Vec3d(n, n, n)
        }
    }
}

internal object ColorSerializer : TypeSerializer<Color> {
    override fun serialize(type: Type, obj: Color?, node: ConfigurationNode) {}

    override fun deserialize(type: Type, node: ConfigurationNode) = Color.fromRGB(node.force<TextColor>().value())
}

private const val COLOR = "color"
private const val SIZE = "size"

internal object DustOptionsSerializer : TypeSerializer<DustOptions> {
    override fun serialize(type: Type, obj: DustOptions?, node: ConfigurationNode) {}

    override fun deserialize(type: Type, node: ConfigurationNode) = DustOptions(
        node.node(COLOR).force(),
        node.node(SIZE).force()
    )
}

internal object BlockDataSerializer : TypeSerializer<BlockData> {
    override fun serialize(type: Type, obj: BlockData?, node: ConfigurationNode) {}

    override fun deserialize(type: Type, node: ConfigurationNode) = try {
        Bukkit.createBlockData(node.force<String>())
    } catch (ex: IllegalArgumentException) {
        throw SerializationException(node, type, "Invalid block data", ex)
    }
}

private const val PARTICLE = "particle"
private const val COUNT = "count"
private const val SPEED = "speed"
private const val DATA = "data"

internal object ParticleEffectSerializer : TypeSerializer<BulletParticleEffect> {
    override fun serialize(type: Type, obj: BulletParticleEffect?, node: ConfigurationNode) {}

    override fun deserialize(type: Type, node: ConfigurationNode): BulletParticleEffect {
        if (node.isMap) {
            val particle = node.node(PARTICLE).force<Particle>()
            val data = when (val dataType = particle.dataType) {
                Void::class.java -> null
                else -> node.node(DATA).get(dataType)
            }
            return BulletParticleEffect(
                particle,
                node.node(COUNT).get { 0 },
                node.node(SIZE).get { Vec3d.ZERO },
                node.node(SPEED).get { 0.0 },
                data
            )
        } else return BulletParticleEffect(
            node.force(), 0, Vec3d.ZERO, 0.0, null
        )
    }
}
