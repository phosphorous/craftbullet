package com.gitlab.aecsocket.craftbullet.paper

import com.gitlab.aecsocket.craftbullet.core.*
import com.jme3.bounding.BoundingBox
import com.jme3.bullet.collision.shapes.BoxCollisionShape
import com.jme3.bullet.collision.shapes.CollisionShape
import com.jme3.bullet.collision.shapes.CompoundCollisionShape
import com.jme3.math.Vector3f
import com.simsilica.mathd.Quatd
import com.simsilica.mathd.Vec3d
import net.minecraft.world.phys.AABB
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.World
import org.bukkit.entity.Entity
import org.bukkit.plugin.Plugin
import org.bukkit.util.EulerAngle
import org.bukkit.util.Vector
import org.bukkit.util.VoxelShape

fun Plugin.scheduleDelayed(delay: Long = 0, task: () -> Unit) {
    Bukkit.getScheduler().scheduleSyncDelayedTask(this, task, delay)
}

fun Plugin.scheduleRepeating(period: Long = 1, delay: Long = 0, task: () -> Unit) {
    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, task, delay, period)
}

fun Vector.vec3f() = Vector3f(x.toFloat(), y.toFloat(), z.toFloat())
fun Vector.vec3d() = Vec3d(x, y, z)

fun Location.vec3f() = Vector3f(x.toFloat(), y.toFloat(), z.toFloat())
fun Location.vec3d() = Vec3d(x, y, z)

fun Vector3f.paper() = Vector(x.toDouble(), y.toDouble(), z.toDouble())
fun Vector3f.location(world: World) = Location(world, x.toDouble(), y.toDouble(), z.toDouble())
fun Vector3f.pose() = EulerAngle(-x.toDouble(), y.toDouble(), z.toDouble())

fun Vec3d.paper() = Vec3d(x, y, z)
fun Vec3d.location(world: World) = Location(world, x, y, z)
fun Vec3d.pose() = EulerAngle(-x, y, z)

fun BoundingBox.paper(): AABB {
    val min = min()
    val max = max()
    return AABB(
        min.x.toDouble(), min.y.toDouble(), min.z.toDouble(),
        max.x.toDouble(), max.y.toDouble(), max.z.toDouble(),
        false // assert our min/maxes are right way round
    )
}

fun BoundingBoxDp.paper(): AABB {
    val min = min()
    val max = max()
    return AABB(
        min.x, min.y, min.z,
        max.x, max.y, max.z,
        false // assert our min/maxes are the right way round
    )
}

fun VoxelShape.createCollision(): CollisionShape {
    val boxes = boundingBoxes
    if (boxes.isEmpty()) return emptyShape

    val shape = CompoundCollisionShape(boxes.size)
    boxes.forEach { box ->
        val extent = box.max.subtract(box.min)
        shape.addChildShape(
            BoxCollisionShape(extent.multiply(0.5).vec3f()),
            box.center.vec3f()
        )
    }

    return shape
}

val Entity.centerOfMass: Vector
    get() = boundingBox.center
val Entity.rotation: Quatd
    get() = Vec3d(0.0, radians(-location.yaw).toDouble(), 0.0).quaternion()
