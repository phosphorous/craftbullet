# DEPRECATED!
## Use [Ignacio](https://gitlab.com/phosphorous/ignacio) instead.

---

<div align="center">

<h1> <a href="https://phosphorous.gitlab.io/craftbullet">
<img src="icon.svg" height="64"> CraftBullet
</a> </h1>

[![Latest version](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39123097%2Fpackages%2Fmaven%2Fcom%2Fgitlab%2Faecsocket%2Fcraftbullet%2Fcraftbullet-core%2Fmaven-metadata.xml)](https://gitlab.com/phosphorous/craftbullet/-/packages/9027066)
[![Pipeline status](https://img.shields.io/gitlab/pipeline-status/phosphorous/craftbullet?branch=main)](https://gitlab.com/phosphorous/craftbullet/-/pipelines/latest)

</div>

Integration of the [Bullet physics library](https://github.com/bulletphysics/bullet3) into a Minecraft server,
using the [Libbulletjme](https://github.com/stephengold/Libbulletjme) library, and providing a developer API
to manipulate physics objects.

### [Quickstart and documentation](https://phosphorous.gitlab.io/craftbullet)

## Downloads for v0.2.4

### [Paper](https://gitlab.com/api/v4/projects/39123097/jobs/artifacts/main/raw/paper/build/libs/craftbullet-paper-0.2.4.jar?job=build)
### [Paper (mojmap)](https://gitlab.com/api/v4/projects/39123097/jobs/artifacts/main/raw/paper/build/libs/craftbullet-paper-0.2.4-dev-all.jar?job=build)
